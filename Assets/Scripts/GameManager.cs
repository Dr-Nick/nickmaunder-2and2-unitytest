﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


[RequireComponent(typeof(Timer))]
public class GameManager : MonoBehaviour 
{
	[SerializeField][Tooltip("The panel that appears when the game is won")]
	private VictoryPanelObject _victoryPanel;

	[SerializeField][Tooltip("Responsible for showing the ingame time display")]
	private GameTimeDisplay _gameTimeDisplay;

	[SerializeField][Tooltip("A particle system that appears when the player dies")]
	private Transform _deathParticleSystem = null;

	[SerializeField][Tooltip("The delay upon entering the scene before the game begins")]
	private float _gameStartTime;

	[SerializeField][Tooltip("The time to wait before player can control a smiley after dying")]
	private float _respawnDelay = 1.0f;

	[SerializeField][Tooltip("The time to wait before player can control a smiley after respawn")]
	private float _respawnInputDelay;

	private Timer _timer;
	private SmileyGuy[] _smileyGuys;
	private bool _hasWonGame = false;


	private void HandleSmileyGuyTriggerEnter(SmileyGuy guy,Collider collider) 
	{
		if (collider.GetComponent<ExitZone>())
		{
			EndGame();
		}
			
		if (collider.GetComponent<Hazard>())
		{
			OnSmileyGuyTouchHazard(guy);
		}
	}

	private void OnSmileyGuyTouchHazard(SmileyGuy player)
	{
		if (_deathParticleSystem != null)
		{
			_deathParticleSystem.gameObject.SetActive(false);
			_deathParticleSystem.gameObject.SetActive(true);
			_deathParticleSystem.position = player.transform.position;
		}
			
		player.Kill();
		CheckIfAllSmileysAreDead();
	}

	private void CheckIfAllSmileysAreDead()
	{
		for (int i = 0; i < _smileyGuys.Length; i++)
		{
			if (_smileyGuys[i] != null)
			{
				if (_smileyGuys[i].IsAlive)
				{
					return;
				}
			}
		}
		//All Smileys are dead - Restart Game
		_timer.enabled = false;
		StartGame(_respawnDelay);
	}


	private IEnumerator RespawnPlayer(SmileyGuy player, float respawnDelay)
	{
		yield return new WaitForSeconds(respawnDelay);
		if (player != null)
		{
			player.Reset();
		}

		yield return new WaitForSeconds(_respawnInputDelay);

		player.enabled = true;

		if (!_hasWonGame)
		{
			_timer.enabled = true;
			_timer.ResetTimer();
		}
	}

	private void SetVictoryPanelActive(bool active)
	{
		if (_victoryPanel != null)
		{
			_victoryPanel.SetActive(active);	
		}
	}

	private void SetVictoryPanelTime()
	{
		if (_victoryPanel != null)
		{
			_victoryPanel.SetTimeText(_timer.TimeElapsedSecondsOnly);
		}
	}
		

	private void StartGame(float respawnTime)
	{
		_hasWonGame = false;
		for (int i = 0; i < _smileyGuys.Length; i++)
		{
			if (_smileyGuys[i] != null)
			{
				_smileyGuys[i].gameObject.SetActive(true);
				StartCoroutine(RespawnPlayer(_smileyGuys[i],respawnTime));
			}
		}
	}
		
	private void EndGame() 
	{
		_timer.enabled = false;
		_hasWonGame = true;

		if ( _gameTimeDisplay != null)
		{
			_gameTimeDisplay.UpdateDisplay(_timer.TimeElapsedSecondsOnly);
		}
			
		SetVictoryPanelTime();
		SetVictoryPanelActive(true);

		for (int i = 0; i < _smileyGuys.Length; i++)
		{
			if (_smileyGuys[i] != null)
			{
				_smileyGuys[i].Win();
			}
		}
	}
				
	IEnumerator Start () 
	{
		_timer = GetComponent<Timer>();
		_timer.enabled = false;
		_timer.ResetTimer();
		SetVictoryPanelActive(false);

		//Set up will only look for active and enabled SmileyGuys
		_smileyGuys = FindObjectsOfType<SmileyGuy>();
		for (int i = 0; i < _smileyGuys.Length; i++)
		{
			_smileyGuys[i].OnEventTriggerEnter += HandleSmileyGuyTriggerEnter;
			//Deactivate the SmileyGuy object - will activate them when ready
			_smileyGuys[i].enabled = false;
			_smileyGuys[i].gameObject.SetActive(false);
		}

		if (_smileyGuys.Length == 0)
		{
			Debug.LogError("There are no SmileyGuys active in the scene. Cannot StartGame without SmileyGuys");
		}
		else
		{
			yield return new WaitForSeconds(_gameStartTime);
			StartGame(0);
		}
	}
		
	void Update () 
	{
		if (_timer.enabled && _gameTimeDisplay != null && !_hasWonGame)
		{
			_gameTimeDisplay.UpdateDisplay(_timer.TimeElapsedSecondsOnly);
		}
	}

	void OnDestroy()
	{
		for (int i = 0; i < _smileyGuys.Length; i++)
		{
			if (_smileyGuys[i] != null)
			{
				_smileyGuys[i].OnEventTriggerEnter += HandleSmileyGuyTriggerEnter;
			}
		}
	}

}