﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class SmileyGuy : MonoBehaviour 
{
	[SerializeField][Tooltip("The distance moved per second")]
	private float _speed;

	[SerializeField]
	private UnityEvent _onSpawn;
	[SerializeField]
	private UnityEvent _onDie;
	[SerializeField]
	private UnityEvent _onWin;


	private bool _isAlive;
	private Vector3 _defaultPosition;
	private Vector2 _inputVector;
	private Vector3 _movementVelocity;
	private Transform _transformCache;
	private Rigidbody _rigidBody;
	private BaseInput _input;

	// Events.
	public delegate void EventTriggerEnter(SmileyGuy guy,Collider collider);
	private event EventTriggerEnter _onEventTriggerEnter;

	public event EventTriggerEnter OnEventTriggerEnter
	{
		add {_onEventTriggerEnter += value;}
		remove {_onEventTriggerEnter -= value;}
	}

	public bool IsAlive
	{
		get {return _isAlive;}
	}
		
	public void Win()
	{
		_isAlive = true;
		this.enabled = false;
		_rigidBody.velocity = new Vector3(0, 0, 0);
		_onWin.Invoke();
	}
		
	public void Reset() 
	{
		_isAlive = true;
		_transformCache.position = _defaultPosition;
		_rigidBody.velocity = new Vector3(0, 0, 0);
		_onSpawn.Invoke();
	}

	public void Kill() 
	{
		enabled = false;
		_isAlive = false;
		_rigidBody.velocity = new Vector3(0, 0, 0);
		_onDie.Invoke();
	}

	private void SetInputMovement(Vector2 input)
	{
		_inputVector = input;
	}
		
	void Awake () 
	{
		_isAlive = true;
		_transformCache = transform;
		_rigidBody = gameObject.GetComponent<Rigidbody>();
		_rigidBody.isKinematic = false;
		_defaultPosition = _transformCache.position;

		_input = GetComponent<BaseInput>();
		if (_input != null)
		{
			_input.OnUpdateDirectionMovement += SetInputMovement;
		}
		else
		{
			Debug.LogError("Input has not been set on " + gameObject.name +
				". Please attach a component of type BaseInput.");
		}
	}

	void OnTriggerEnter(Collider collider) 
	{
		if (_onEventTriggerEnter != null)
		{
			_onEventTriggerEnter(this,collider);
		}
 	}
		
	void FixedUpdate()
	{
		if (!_isAlive) return;

		_movementVelocity.x = Mathf.Clamp(_inputVector.x, -1f,1f);
		_movementVelocity.y = Mathf.Clamp(_inputVector.y, -1f,1f);
		_movementVelocity.z = 0;

		if (_movementVelocity.sqrMagnitude > 1f)
		{
			_movementVelocity.Normalize();
		}
			
		_movementVelocity *= _speed;
		_rigidBody.velocity = _movementVelocity;
	}

	void OnDestroy()
	{
		if (_input != null)
		{
			_input.OnUpdateDirectionMovement -= SetInputMovement;
		}
	}
		
}