﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
 
public class MenuManager : MonoBehaviour 
{
	public void PressButtonStart() 
	{
		SceneManager.LoadScene(1);
	}

	public void PressButtonQuit() 
	{
		#if UNITY_EDITOR
		Debug.Log("The quit command was issued.");
		#else
		Application.Quit();
		#endif
	}
}