﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonRestart : MonoBehaviour 
{
	public void Restart() 
	{
		SceneManager.LoadScene(0);
	}
}