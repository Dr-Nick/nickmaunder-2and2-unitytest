﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Renderer))]
public class AnimateTextureOffset : MonoBehaviour 
{

	[SerializeField][Tooltip("The ammount by which the texture is offset every second")]
	private Vector2 _offsetVelocity;

	[SerializeField][Tooltip("The property name which adjusts the offset of the texture")]
	private string _offsetPropertyName = "_MainTex";

	private Renderer _renderer;
	private Material _materialRef;

	void Awake () 
	{
		_renderer = GetComponent<Renderer>();
	}

	void OnEnable()
	{
		if (_materialRef == null)
		{
			if (_renderer.material != null)
			{
				_materialRef = _renderer.material;
				return;
			}
		}
		else
		{
			return;
		}
		//Renderer not set up properly. Disable component
		this.enabled = false;
	}
		
	void Update () 
	{
		_materialRef.SetTextureOffset(_offsetPropertyName, 
			_materialRef.GetTextureOffset(_offsetPropertyName) +_offsetVelocity * Time.deltaTime);
	}

	void OnDestroy()
	{
		if (_materialRef != null)
		{
			//Destroy material so it doesn't stay stored in memory
			Destroy(_materialRef);
		}

	}
}
