﻿using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class VictoryPanelObject : MonoBehaviour 
{
	[SerializeField][Tooltip("The number of seconds that have elapsed")]
	private Text _victoryPanelTimeText; 

	private StringBuilder _stringBuilder = new StringBuilder();



	public void SetActive(bool active)
	{
		gameObject.SetActive(active);
	}

	public void SetTimeText(int timeSeconds)
	{
		if (_victoryPanelTimeText != null)
		{
			_stringBuilder.Length = 0;
			_stringBuilder.Append(timeSeconds.ToString());
			_stringBuilder.Append((timeSeconds == 1) ? " second" :  " seconds");
			_victoryPanelTimeText.text = _stringBuilder.ToString();
		}
	}
}
