﻿using UnityEngine;
using UnityEngine.UI;


public class GameTimeDisplay : MonoBehaviour 
{
	private const string STR_SECOND = "second";
	private const string STR_SECONDS = "seconds";


	[SerializeField][Tooltip("The number of seconds that have elapsed")]
	private Text _timeText;
	[SerializeField][Tooltip("This diplays the 'seconds' caption")]
	private Text _timeTextCaption;

	private int _secondsElapsed = 0;

	public void UpdateDisplay(int timeSeconds)
	{
		if (_secondsElapsed != timeSeconds)
		{
			if (_timeText != null)
			{
				_timeText.text = timeSeconds.ToString();
			}

			//Set the second/s caption text
			if (_timeTextCaption != null)
			{
				if (_secondsElapsed == 1 && timeSeconds != 1)
				{
					_timeTextCaption.text = STR_SECONDS; 
				}
				else if (timeSeconds == 1)
				{
					_timeTextCaption.text = STR_SECOND; 
				}
			}
			_secondsElapsed = timeSeconds;
		}
	} 
		
}
