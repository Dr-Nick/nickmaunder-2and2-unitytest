﻿using UnityEngine;
using System;
using System.Collections;

public abstract class BaseInput : MonoBehaviour 
{
	protected Action<Vector2> _onUpdateDirectionMovement;

	public event Action<Vector2> OnUpdateDirectionMovement
	{ 
		add{_onUpdateDirectionMovement += value; } remove{_onUpdateDirectionMovement -= value; } 
	}
}
