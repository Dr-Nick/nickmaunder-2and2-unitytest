﻿using UnityEngine;
using System.Collections;

public class KeyboardInput : BaseInput 
{
	private Vector2 _inputVector;

	private void Update()
	{
		_inputVector = Vector2.zero;

		if (Input.GetKey(KeyCode.W)) _inputVector.y += 1f;
		if (Input.GetKey(KeyCode.A)) _inputVector.x += -1f;
		if (Input.GetKey(KeyCode.S)) _inputVector.y += -1f;
		if (Input.GetKey(KeyCode.D)) _inputVector.x += 1f;

		_onUpdateDirectionMovement(_inputVector);
	}

}
