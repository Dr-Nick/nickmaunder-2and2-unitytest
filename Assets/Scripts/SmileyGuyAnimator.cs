﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(SmileyGuy),typeof(Animator))]
public class SmileyGuyAnimator : MonoBehaviour {

	[SerializeField]
	private string _aliveStateString = string.Empty;
	[SerializeField]
	private string _winTriggerString = string.Empty;


	Animator _animator;

	public void OnRespawn()
	{
		_animator.SetBool(_aliveStateString,true);
	}
		
	public void OnDie()
	{
		_animator.SetBool(_aliveStateString,false);
	}

	public void OnWin()
	{
		_animator.SetTrigger(_winTriggerString);
	}



	void Start()
	{
		_animator = GetComponent<Animator>();
	}
}
