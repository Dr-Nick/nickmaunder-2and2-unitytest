﻿using UnityEngine;
using System;
using System.Collections;

public class Timer : MonoBehaviour 
{
	private TimeSpan _timespan = new TimeSpan();

	public int TimeElapsedSecondsOnly
	{
		get {return _timespan.Seconds;}
	}
		
	public string TimeFormattedStringSecondsOnly
	{
		get {return _timespan.Seconds.ToString();}
	}
					
	public void ResetTimer()
	{
		_timespan = new TimeSpan();
	}
		
	void Update () 
	{
		_timespan = _timespan.Add(TimeSpan.FromSeconds(Time.unscaledDeltaTime));
	}

}